/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.transfer;

import be.yildizgames.common.os.factory.OperatingSystems;
import be.yildizgames.common.os.scripts.ScriptRunnerFactory;
import be.yildizgames.launcher.client.PathUtil;
import be.yildizgames.launcher.client.event.EventFactory;
import be.yildizgames.launcher.client.event.EventManager;
import be.yildizgames.launcher.client.network.NetworkClient;
import be.yildizgames.launcher.client.view.TransferCompleted;
import be.yildizgames.launcher.client.view.TranslationData;
import be.yildizgames.launcher.shared.files.FileDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Paths;
import java.util.Set;

/**
 * @author Grégory Van den Borre
 */
public class Launcher implements Update {

    private static final Logger LOGGER = LoggerFactory.getLogger(Launcher.class);

    private final NetworkClient client;

    public Launcher(NetworkClient client) {
        this.client = client;
    }

    @Override
    public boolean isUpToDate() {
        LOGGER.debug("Checking launcher version");
        try {
            String result = this.client.retrieveText("launcher/" + OperatingSystems.getCurrent().getName() + "/version-launcher");
            String implementationVersion = this.getClass().getPackage().getImplementationVersion();
            String version = implementationVersion == null ? "0.0.0-DEV" : implementationVersion.trim();
            LOGGER.debug("Version: {} - last: {}", version, result);
            return result.trim().equals(version);
        } catch (Exception e) {
            LOGGER.error("An error occurred when checking update.", e);
            EventManager.getInstance().fireError(TranslationData.ERROR_GENERAL_UPDATE);
            return true;
        }
    }

    @Override
    public Set<FileDescription> computeFileToGet() {
        return Set.of(new FileDescription("last_launcher.exe", 0, 7700000));
    }

    @Override
    public TransferCompleted computeTransferCompleted() {
        return () -> {
            LOGGER.info("Update launcher");
            try {
                ScriptRunnerFactory.runnerForCurrent().run(PathUtil.SCRIPT_LAUNCHER_UPDATE);
            } catch (Exception e) {
                LOGGER.error("Unhandled exception", e);
            }
            System.exit(0);
            EventManager.getInstance().fireEvent(EventFactory.close());
        };
    }

    @Override
    public String getName() {
        return "launcher";
    }

    @Override
    public void getUpdate(Set<FileDescription> files) {
        files.forEach(f -> client.request("launcher", f, Paths.get(f.name)));
    }

    @Override
    public boolean isBlocking() {
        return true;
    }
}
