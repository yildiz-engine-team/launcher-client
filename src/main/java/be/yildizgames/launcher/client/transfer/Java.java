/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.transfer;

import be.yildizgames.common.client.translation.TranslationKey;
import be.yildizgames.common.compression.CompressionFactory;
import be.yildizgames.common.exception.technical.ResourceMissingException;
import be.yildizgames.common.file.FileResource;
import be.yildizgames.common.logging.LogFactory;
import be.yildizgames.common.os.factory.OperatingSystems;
import be.yildizgames.common.os.scripts.ScriptRunnerFactory;
import be.yildizgames.launcher.client.PathUtil;
import be.yildizgames.launcher.client.event.EventFactory;
import be.yildizgames.launcher.client.event.EventManager;
import be.yildizgames.launcher.client.network.NetworkClient;
import be.yildizgames.launcher.client.view.TransferCompleted;
import be.yildizgames.launcher.client.view.TranslationData;
import be.yildizgames.launcher.shared.files.FileDescription;
import org.slf4j.Logger;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

/**
 * @author Grégory Van den Borre
 */
public class Java implements Update {

    private static final Logger LOGGER = LogFactory.getInstance().getLogger(Java.class);

    private final NetworkClient client;

    Java(NetworkClient client) {
        this.client = client;
    }


    @Override
    public boolean isUpToDate() {
        LOGGER.debug("Checking java version");
        long crc32;
        try {
            if (OperatingSystems.getCurrent().getName().contains("linux")) {
                crc32 = FileResource.findResource("../jre/bin/java").getCrc32();
            } else {
                crc32 = FileResource.findResource("../jre/bin/java.exe").getCrc32();
            }
            return this.checkVersion(crc32);
        } catch (ResourceMissingException e) {
            EventManager.getInstance().fireWarning(TranslationKey.get("error.java.exists"));
            LOGGER.debug("Cannot find {}", Paths.get("../jre/bin/java").toAbsolutePath());
            LOGGER.debug("Need update for {}", this.getName());
            EventManager.getInstance().fireEvent(EventFactory.needUpdate(this.getName()));
            return false;
        }

    }

    private boolean checkVersion(long crc32) {
        try {
            String result = this.client.retrieveText("jre/" + OperatingSystems.getCurrent().getName() + "/version-java");
            long v = Long.parseLong(result.trim());
            LOGGER.debug("Java: " + crc32 + " - last: " + v);
            return v == crc32;
        } catch (Exception e) {
            EventManager.getInstance().fireError(TranslationKey.get("error.java.content"));
            LOGGER.debug("Need update for {}", this.getName());
            return true;
        }
    }

    @Override
    public Set<FileDescription> computeFileToGet() {
        return Set.of(new FileDescription("jre.zip", 0, 50000000));
    }

    @Override
    public TransferCompleted computeTransferCompleted() {
    return (() -> {
        try {
            Path zipFile = Paths.get("jre.zip");
            CompressionFactory.zipUnpacker().unpack(zipFile, Paths.get("").getParent().resolve("last_jre"), false);
        } catch (Exception e) {
            EventManager.getInstance().fireError(TranslationData.ERROR_ZIP_OPEN);
        }
        LOGGER.info("Update JVM");
        try {
            ScriptRunnerFactory.runnerForCurrent().run(PathUtil.SCRIPT_JAVA_UPDATE);
            EventManager.getInstance().fireEvent(EventFactory.close());
        } catch (Exception e) {
            LOGGER.error("Error", e);
        }
        LOGGER.info("Closing");
        EventManager.getInstance().fireEvent(EventFactory.close());
    });
    }

    @Override
    public String getName() {
        return "java";
    }

    @Override
    public void getUpdate(Set<FileDescription> files) {
        files.forEach(f -> client.request("jre", f, Paths.get(f.name)));
    }

    @Override
    public boolean isBlocking() {
        return true;
    }
}
