/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.transfer;

import be.yildizgames.launcher.client.ThreadManager;
import be.yildizgames.launcher.client.event.*;
import be.yildizgames.launcher.client.game.SupportedGame;
import be.yildizgames.launcher.client.game.SupportedGameList;
import be.yildizgames.launcher.client.network.NetworkClient;
import be.yildizgames.launcher.client.view.TransferCompleted;
import be.yildizgames.launcher.client.view.TranslationData;
import be.yildizgames.launcher.shared.files.FileDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Grégory Van den Borre
 */
public class UpdateManager implements EventListener, Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateManager.class);

    private final Map<String, Update> updates = new LinkedHashMap<>();

    private final SupportedGameList games;

    private final NetworkClient client;

    public UpdateManager(NetworkClient client, SupportedGameList games) {
        this.client = client;
        this.registerUpdate(new Java(client));
        this.registerUpdate(new Launcher(client));
        for(SupportedGame game : games) {
            if(game.isInstalled()) {
                this.registerUpdate(new GameFile(game.getName(), client));
            }
        }
        this.games = games;

        EventManager.getInstance()
                .observe(EventConstant.STARTING_UPDATE, this)
                .observe(EventConstant.INSTALL_GAME, this);
    }

    private void registerUpdate(Update u) {
        this.updates.put(u.getName(), u);
    }

    @Override
    public void run() {
        LOGGER.debug("Starting check for update.");
        boolean blockingUpdate = false;
        for(Update u : this.updates.values()){
            if(!u.isUpToDate()) {
                //FIXME send only one event(blocking)
                EventManager.getInstance().fireEvent(EventFactory.needUpdate(u.getName()));
                if (u.isBlocking()) {
                    EventManager.getInstance().fireEvent(EventFactory.blockingUpdate(u.getName()));
                    blockingUpdate = true;
                    break;
                }
            } else {
                EventManager.getInstance().fireEvent(EventFactory.ready(u.getName()));
            }
        }
        if(!blockingUpdate) {
            this.games.getFirst().ifPresent(g -> EventManager.getInstance().fireEvent(EventFactory.activateGame(g.getName())));
        }
        LOGGER.debug("Check for update complete.");
    }

    private void startUpdate(String type) {
        LOGGER.debug("Start update {}", type);
        try {
            Set<FileDescription> files = this.updates.get(type).computeFileToGet();
            this.updates.get(type).getUpdate(files);
            TransferCompleted tc = this.updates.get(type).computeTransferCompleted();
            tc.complete();
        } catch (Exception e) {
            LOGGER.error("Error while updating", e);
            EventManager.getInstance().fireError(TranslationData.ERROR_GENERAL_UPDATE);
        }
    }

    @Override
    public void listen(Event event) {
        LOGGER.debug("Event received: {}", event);
        if(event.name.equals(EventConstant.STARTING_UPDATE)) {
            ThreadManager.runAsync(() -> this.startUpdate(event.getProperty(EventConstant.PROPERTY_TYPE)));
        } else if(event.name.equals(EventConstant.INSTALL_GAME)) {
            games.byName(event.getProperty(EventConstant.PROPERTY_TYPE)).ifPresent(g -> {
                g.setInstalled(true);
                this.registerUpdate(new GameFile(g.getName(), client));
                ThreadManager.runAsync(this);
            });
        }
    }
}
