/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.transfer;

import be.yildizgames.common.file.exception.FileDeletionException;
import be.yildizgames.common.os.OperatingSystem;
import be.yildizgames.common.os.factory.OperatingSystems;
import be.yildizgames.launcher.client.event.EventFactory;
import be.yildizgames.launcher.client.event.EventManager;
import be.yildizgames.launcher.client.network.NetworkClient;
import be.yildizgames.launcher.client.view.TransferCompleted;
import be.yildizgames.launcher.client.view.TranslationData;
import be.yildizgames.launcher.shared.constant.Constants;
import be.yildizgames.launcher.shared.files.FileDescription;
import be.yildizgames.launcher.shared.files.ListBuilder;
import be.yildizgames.launcher.shared.files.ListComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

/**
 * @author Grégory Van den Borre
 */
public class GameFile implements Update {

    private static final Logger LOGGER = LoggerFactory.getLogger(GameFile.class);

    private final NetworkClient client;
    private final OperatingSystem system = OperatingSystems.getCurrent();
    private final String name;
    private Set<FileDescription> filesToUpdate;

    GameFile(String gameName, NetworkClient client) {
        this.client = client;
        this.name = gameName;
    }

    @Override
    public boolean isUpToDate() {
        try {
            this.filesToUpdate = this.checkFiles(this.name);
            return filesToUpdate.isEmpty();
        } catch (Exception e) {
            LOGGER.error("An error occurred when checking update.", e);
            EventManager.getInstance().fireError(TranslationData.ERROR_GENERAL_UPDATE);
            return true;
        }
    }

    @Override
    public Set<FileDescription> computeFileToGet() {
        return this.filesToUpdate;
    }

    @Override
    public TransferCompleted computeTransferCompleted() {
        return () -> {};
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void getUpdate(Set<FileDescription> files) {
        files.forEach(f -> client.request(this.name, f, Paths.get(this.name, f.name)));
        EventManager.getInstance().fireEvent(EventFactory.ready(this.name));
    }

    @Override
    public boolean isBlocking() {
        return false;
    }

    /**
     * Check if the game files on server matches the files on client.
     *
     * @param fileDirectory
     *            Directory to check on client.
     * @return The list of missing or corrupted files.
     * @throws Exception If something went wrong.
     */
    private Set<FileDescription> checkFiles(final String fileDirectory) throws Exception {
        LOGGER.debug("Checking game files {}", this.name);
        String validList = this.retrieveList().replaceAll("/yildiz-files/" + system.getName() + "/", "");
        Path dir = Paths.get(fileDirectory);
        Files.createDirectories(dir);
        ListBuilder listBuilder = new ListBuilder(dir);
        String localList = listBuilder.createList();
        ListComparator comparator = new ListComparator(validList, localList);
        final Set<FileDescription> additionalFileList = comparator.getAdditional();
        additionalFileList.stream()
                .map(f -> f.name)
                .filter(f -> f.contains("config.properties"))
                .forEach(f -> {
                    try {
                        Files.delete(Paths.get(f));
                    } catch (IOException e) {
                        throw new FileDeletionException(e);
                    }
                });
        return comparator.getMissing();
    }

    /**
     * Request the server for a list of files. The list will be written in the
     * file root directory.
     *
     * @return The list.
     * @throws Exception If something went wrong.
     */
    private String retrieveList() throws Exception {
        return this.client.retrieveText(this.name + "/" + system.getName() + "/" + Constants.LIST);
    }
}
