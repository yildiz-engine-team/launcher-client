/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.script;

import be.yildizgames.common.os.scripts.ScriptBuilderFactory;
import be.yildizgames.launcher.client.PathUtil;
import be.yildizgames.launcher.client.game.SupportedGame;
import be.yildizgames.launcher.client.game.SupportedGameList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * This class will create(or update) the external scripts used by the launcher.
 * @author Grégory Van den Borre
 */
public class ExternalScriptBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalScriptBuilder.class);

    private static final String LAUNCHER_OLD_BIN = "last-launcher";

    private static final String LAUNCHER_BIN = "yildiz-launcher";

    private static final String JAVA_DIR = "jre";

    private static final String JAVA_OLD_DIR = "last_jre";

    /**
     * Generate the scripts to launch external registered games.
     * Side effects: Create files on the disk.
     * @param games Registered games.
     */
    public void run(SupportedGameList games) {
        try {
            generateGameLauncher(games);
            generateJavaUpdater();
            generateLauncherUpdater();
        } catch (IOException e) {
            LOGGER.error("Unhandled exception", e);
        }
    }

    private void generateLauncherUpdater() throws IOException {
        ScriptBuilderFactory.builderForCurrent()
                .deleteInCurrentDir(LAUNCHER_BIN)
                .renameInCurrentDir(LAUNCHER_OLD_BIN, LAUNCHER_BIN)
                .startBin(LAUNCHER_BIN)
                .exit()
                .generateFile(PathUtil.SCRIPT_LAUNCHER_UPDATE);
    }

    private void generateJavaUpdater() throws IOException {
        ScriptBuilderFactory.builderForCurrent()
                .deleteInParentDir(JAVA_DIR)
                .renameInParentDir(JAVA_OLD_DIR, JAVA_DIR)
                .startBin(LAUNCHER_BIN)
                .generateFile(PathUtil.SCRIPT_JAVA_UPDATE);
    }

    private void generateGameLauncher(SupportedGameList games) throws IOException {
        for(SupportedGame game : games) {
            ScriptBuilderFactory.builderForCurrent()
                    .goToDirectory(game.getName())
                    .startBin(game.getName())
                    .generateFile("launch_" + game.getName());
        }
    }
}
