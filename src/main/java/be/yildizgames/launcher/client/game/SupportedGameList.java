/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.game;

import be.yildizgames.launcher.client.event.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author Grégory Van den Borre
 */
public class SupportedGameList implements Iterable<SupportedGame>, EventListener {

    private final Logger Logger = LoggerFactory.getLogger(this.getClass());

    private final Map<String, SupportedGame> list = new LinkedHashMap<>();

    private SupportedGameList() {
        super();

        EventManager.getInstance()
                .observe(EventConstant.READY, this)
                .observe(EventConstant.CHECKING, this)
                .observe(EventConstant.NEED_UPDATE, this)
                .observe(EventConstant.STARTING_UPDATE, this)
                .observe(EventConstant.RECEIVE, this);
    }

    public static SupportedGameList create() {
        return new SupportedGameList();
    }

    /**
     * Add a new Supported game.
     * Side effects: none.
     * Modify: this.
     * @param game Game to support, cannot be null.
     * @return This object for chaining.
     * @throws AssertionError if game is null.
     */
    public final SupportedGameList register(final SupportedGame game) {
        assert game != null;
        this.list.put(game.getName(), game);
        EventManager.getInstance().fireEvent(EventFactory.registerGame(game, this.list.size() - 1));
        return this;
    }

    public final Optional<SupportedGame> byName(String name) {
        return Optional.ofNullable(this.list.get(name));
    }

    @Override
    public Iterator<SupportedGame> iterator() {
        return list.values().iterator();
    }

    public Optional<SupportedGame> getFirstInstalled() {
        return list.values()
                .stream()
                .filter(SupportedGame::isInstalled)
                .findFirst();
    }

    public Optional<SupportedGame> getFirst() {
        return list.values()
                .stream()
                .findFirst();
    }

    @Override
    public void listen(Event event) {
        Logger.debug("Event received: {}", event);
        Optional.ofNullable(this.list.get(event.getProperty(EventConstant.PROPERTY_TYPE)))
                .ifPresent(s -> s.addEvent(event));
    }
}
