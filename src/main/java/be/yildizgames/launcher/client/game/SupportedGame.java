/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.game;

import be.yildizgames.common.logging.LogFactory;
import be.yildizgames.launcher.client.event.Event;
import be.yildizgames.launcher.client.event.EventRepository;
import org.slf4j.Logger;

/**
 * @author Grégory Van den Borre
 */
public class SupportedGame {

    private static final Logger LOGGER = LogFactory.getInstance().getLogger(SupportedGame.class);

    private final String name;

    private final String banner;

    private final String icon;

    private final String hoverIcon;

    private final EventRepository repository = new EventRepository();

    /**
     * Flag to check if this game must be installed
     */
    private boolean active;

    private boolean installed;

    public SupportedGame(String name, String banner, String icon, String hoverIcon, boolean active, boolean installed) {
        super();
        this.name = name;
        this.banner = banner;
        this.icon = icon;
        this.hoverIcon = hoverIcon;
        this.active = active;
        this.installed = installed;
    }

    public final String getName() {
        return name;
    }

    public final String getBanner() {
        return banner;
    }

    public final String getIcon() {
        return icon;
    }

    public final String getHoverIcon() {
        return hoverIcon;
    }

    public final boolean isActive() {
        return active;
    }

    public final void setActive(boolean active) {
        this.active = active;
    }

    public final boolean isInstalled() {
        return installed;
    }

    public final void setInstalled(boolean installed) {
        this.installed = installed;
    }

    public final void addEvent(Event e) {
        LOGGER.debug("Add event {} for {}.", e, this.name);
        this.repository.put(e);
    }
}
