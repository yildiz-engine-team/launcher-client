package be.yildizgames.launcher.client.view.state;

import be.yildizgames.launcher.client.view.Refreshable;

public interface StateWithContext extends State, Refreshable {

    String getName();

    default void setContext(String context) {

    }

}
