/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.view;

import be.yildizgames.common.client.config.Configuration;
import be.yildizgames.common.client.translation.Translation;
import be.yildizgames.common.util.language.LanguageValue;
import be.yildizgames.launcher.client.event.EventFactory;
import be.yildizgames.launcher.client.event.EventManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import java.util.Arrays;

/**
 * @author Grégory Van den Borre
 */
public class OptionWindow {

    public OptionWindow(Display display) {
        final Shell opt = new Shell(display, SWT.CLOSE);
        ViewTemplate template = new ViewTemplate(opt);
        opt.setBackgroundMode(SWT.INHERIT_DEFAULT);
        opt.setBackground(SwtConverter.from(template.getBackgroundColor()));
        opt.setText(Translation.getInstance().translate(TranslationData.WINDOW_OPTION));
        opt.setSize(template.divideWindowWidth(2), template.divideWindowWidth(2));
        opt.setVisible(true);
        final Combo language = new Combo(opt, SWT.READ_ONLY);
        String[] languageNames = Arrays
                .stream(LanguageValue.values())
                .map(l -> l.description)
                .toArray(String[]::new);
        language.setItems(languageNames);
        language.setBounds(SwtConverter.from(template.getLanguageCoordinate()));
        Button close = template.createButton(template.getOptionLeftButtonCoordinates(), Translation.getInstance().translate(TranslationData.BUTTON_CANCEL));
        close.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseDown(MouseEvent arg0) {
                opt.close();
            }
        });
        language.select(Configuration.getInstance().getLanguage().getId());

        Button save = template.createButton(template.getOptionRightButtonCoordinates(), Translation.getInstance().translate(TranslationData.BUTTON_SAVE));
        save.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseDown(final MouseEvent arg0) {
                Translation.getInstance().chooseLanguage(LanguageValue.values()[language.getSelectionIndex()]);
                opt.close();
                EventManager.getInstance().fireEvent(EventFactory.option());
            }
        });
    }
}
