/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.view.state;

import be.yildizgames.common.client.translation.Translation;
import be.yildizgames.launcher.client.event.*;
import be.yildizgames.launcher.client.game.SupportedGameList;
import be.yildizgames.launcher.client.view.OptionWindow;
import be.yildizgames.launcher.client.view.TranslationData;
import be.yildizgames.launcher.client.view.ViewTemplate;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author Grégory Van den Borre
 */
public class Ready implements State, EventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(Ready.class);

    /**
     * Button to start the game.
     */
    private final Button play;

    private final Button install;

    private final ViewTemplate template;

    /**
     * Button to display the options.
     */
    private final Button options;

    private final SupportedGameList games;

    private final EventRepository repository = new EventRepository();

    private String current = "";
    private boolean blocked;

    public Ready(ViewTemplate template, SupportedGameList games) {
        this.template = template;
        this.games = games;
        this.play = this.template.createButton(this.template.getLeftButtonCoordinates(), Translation.getInstance().translate(TranslationData.BUTTON_PLAY));
        this.play.setVisible(false);
        this.options = this.template.createButton(this.template.getRightButtonCoordinates(),Translation.getInstance().translate(TranslationData.BUTTON_OPTION));
        this.options.setVisible(false);
        this.options.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseDown(final MouseEvent event) {
                new OptionWindow(Display.getCurrent());
            }
        });
        LaunchMouseListener launchMouseListener = new LaunchMouseListener();
        this.play.addMouseListener(launchMouseListener);
        this.install = this.template.createButton(this.template.getRightButtonCoordinates(),Translation.getInstance().translate(TranslationData.BUTTON_INSTALL));
        this.install.addMouseListener(new InstallMouseListener());
    }

    @Override
    public String getName() {
        return EventConstant.READY;
    }

    @Override
    public void activate() {
        this.refresh();
    }

    @Override
    public void setContext(String context) {
        this.current = context;
    }

    @Override
    public void deactivate() {
        this.options.setVisible(false);
        this.play.setVisible(false);
        this.install.setVisible(false);
    }

    @Override
    public void refresh() {
        LOGGER.debug("refresh {} : current game : {}", this.getName(), this.current);
        this.games.byName(this.current).ifPresent(game -> {
            this.options.setVisible(game.isInstalled());
            this.play.setVisible(game.isInstalled());
            this.install.setVisible(!game.isInstalled());
        });
        this.template.setText(this.play, Translation.getInstance().translate(TranslationData.BUTTON_PLAY));
        this.template.setText(this.options, Translation.getInstance().translate(TranslationData.BUTTON_OPTION));
        this.template.setText(this.install, Translation.getInstance().translate(TranslationData.BUTTON_INSTALL));
    }

    @Override
    public void listen(Event event) {
        LOGGER.debug("Event received: {}", event);
        switch (event.name) {
            case EventConstant.ACTIVE_GAME:
                if(!this.blocked) {
                    this.current = event.getProperty(EventConstant.PROPERTY_TYPE);
                    this.refresh();
                }
                break;
            case EventConstant.BLOCKING_UPDATE:
                this.blocked = true;
                break;
        }
        this.repository.put(event);
    }

    @Override
    public String toString() {
        return this.getName();
    }

    private final class LaunchMouseListener extends MouseAdapter {

        private LaunchMouseListener() {
            super();
        }

        @Override
        public void mouseDown(final MouseEvent event) {
            try {
                if(Files.exists(Paths.get("files",current + ".exe"))) {
                    Runtime.getRuntime().exec("cmd /c start launch_" + current + ".bat");
                } else {
                    EventManager.getInstance().fireError(TranslationData.ERROR_FILE_GAME_404);
                }
            } catch (IOException e) {
                LOGGER.error("Error launching", e);
            }
            EventManager.getInstance().fireEvent(EventFactory.close());
        }
    }

    private final class InstallMouseListener extends MouseAdapter {

        private InstallMouseListener() {
            super();
        }

        @Override
        public void mouseDown(final MouseEvent event) {
            EventManager.getInstance().fireEvent(EventFactory.install(current));
        }
    }
}
