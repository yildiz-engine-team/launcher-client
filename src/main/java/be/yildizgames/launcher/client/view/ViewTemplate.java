/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.view;

import be.yildizgames.module.color.Color;
import be.yildizgames.module.coordinate.Coordinates;
import be.yildizgames.module.coordinate.Position;
import be.yildizgames.module.window.ScreenSize;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackListener;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * @author Grégory Van den Borre
 */
public class ViewTemplate {

    private final Shell shell;

    private final ScreenSize windowSize;

    private final ScreenSize screenSize;

    private final int buttonTextWidth;

    private final int buttonTextHeight;

    private final Font font;

    public ViewTemplate(Shell shell) {
        this.shell = shell;
        this.windowSize = new ScreenSize(
                (int)(shell.getDisplay().getPrimaryMonitor().getBounds().width * 0.8),
                (int)(shell.getDisplay().getPrimaryMonitor().getBounds().height * 0.8));
        this.screenSize = new ScreenSize(
                shell.getDisplay().getPrimaryMonitor().getBounds().width,
                shell.getDisplay().getPrimaryMonitor().getBounds().height);
        this.buttonTextWidth = this.divideWindowWidth(6);
        this.buttonTextHeight = this.divideWindowHeight(20);
        this.font = new Font(shell.getDisplay(), new FontData(this.getFontName(), this.divideWindowHeight(60) ,SWT.NONE));
    }

    int divideWindowWidth(float value) {
        return (int)(this.shell.getSize().x / value);
    }

    int divideWindowHeight(float value) {
        return (int)(this.shell.getSize().y / value);
    }

    public final Label createLabel(Coordinates c) {
        Label l = new Label(this.shell, SWT.SMOOTH);
        l.setBounds(c.left, c.top, 150, 30);
        l.setFont(this.font);
        l.setForeground(SwtConverter.from(this.getFontColor()));
        return l;
    }

    public final Label createLabel(int x, int y) {
        Label l = new Label(this.shell, SWT.SMOOTH);
        l.setBounds(x, y, this.shell.getSize().x, this.shell.getSize().y);
        l.setFont(this.font);
        l.setForeground(SwtConverter.from(this.getFontColor()));
        return l;
    }

    public final Coordinates getWindowCoordinates() {
        int left = (this.screenSize.width - this.windowSize.width) >> 1;
        int top = (this.screenSize.height - this.windowSize.height) >> 1;
        return new Coordinates(windowSize.width, windowSize.height, left, top);
    }

    public final Position getLeftButtonCoordinates() {
        return new Position(this.divideWindowWidth(1.77f), this.divideWindowHeight(1.15f));
    }

    public final  Position getRightButtonCoordinates() {
        return new Position(this.divideWindowWidth(1.34f), this.divideWindowHeight(1.15f));
    }

    final Position getOptionLeftButtonCoordinates() {
        return new Position(this.divideWindowWidth(3.4f), this.divideWindowWidth(2.5f));
    }

    final Position getOptionRightButtonCoordinates() {
        return new Position(this.divideWindowWidth(1.7f), this.divideWindowWidth(2.5f));
    }

    final Coordinates getLogoCoordinates() {
        return new Coordinates(
                this.divideWindowWidth(3),
                this.divideWindowHeight(12),
                this.divideWindowWidth(40),
                this.divideWindowHeight(12)
        );
    }

    final Coordinates getQuitButtonCoordinates() {
        return new Coordinates(
                40,
                40,
                this.windowSize.width - 40,
                0);
    }

    private WidgetSize getButtonIconSize() {
        int value = divideWindowWidth(20);
        return new WidgetSize(value, value);
    }

    private WidgetSize getButtonGameIconSize() {
        int value = divideWindowWidth(15);
        return new WidgetSize(value, value);
    }

    final Coordinates getTextCoordinates() {
        Coordinates gameButtonCoordinates = this.getGameIconCoordinate(0);
        int gameButtonRight = gameButtonCoordinates.left + gameButtonCoordinates.width;
        int browserBorder = this.divideWindowWidth(16);
        return new Coordinates(
                this.windowSize.width - gameButtonRight - 2 * browserBorder,
                this.divideWindowHeight(2),
                browserBorder + gameButtonRight,
                this.divideWindowHeight(3));
    }

    public final Coordinates getProgressCoordinate() {
        return new Coordinates(this.divideWindowWidth(1.5f), this.divideWindowHeight(30), this.divideWindowWidth(10), this.divideWindowHeight(1.1f));
    }

    public final Coordinates getProgressNameCoordinate() {
        return new Coordinates(0, 0, this.divideWindowWidth(15), this.divideWindowHeight(1.1f));
    }

    public final ProgressBar createProgressBar() {
        return new ProgressBar(this.shell, SWT.None);
    }

    public final Coordinates getLanguageCoordinate() {
        return new Coordinates(this.divideWindowWidth(4), this.divideWindowWidth(15), this.divideWindowWidth(5), this.divideWindowWidth(12));
    }

    public final Coordinates getGameIconCoordinate(int position) {
        int iconSize = this.getButtonGameIconSize().width;
        int space = this.divideWindowHeight(35);
        return new Coordinates(iconSize, iconSize, this.divideWindowWidth(40), ((position + 1) * (iconSize + space)));
    }

    public Text createTextArea() {
        Text textArea = new Text(this.shell, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
        textArea.setBackground(SwtConverter.from(this.getBackgroundColor()));
        textArea.setFont(this.font);
        //Color white gives an unexpected result, same for color 255,255,255, this hack solves it.
        textArea.setForeground(SwtConverter.from(Color.rgb(254,255,255)));
        return textArea;
    }

    public Button createButton(Position p, String text) {
        Button button = new Button(this.shell, SWT.SMOOTH);
        button.setBounds(p.left, p.top, this.buttonTextWidth, this.buttonTextHeight);
        this.setText(button, text);
        button.setText(text);
        return button;
    }

    public void setText(final Button b, final String text) {
        Image background = new Image(b.getShell().getDisplay(), this.buttonTextWidth, this.buttonTextHeight);
        org.eclipse.swt.graphics.Color c = SwtConverter.from(this.getButtonColor());
        org.eclipse.swt.graphics.Color h = SwtConverter.from(this.getButtonHighlightColor());
        org.eclipse.swt.graphics.Color t = SwtConverter.from(this.getButtonTextColor());
        ViewTemplate.ButtonHover bh = new ViewTemplate.ButtonHover();
        b.addMouseTrackListener(new MouseTrackListener() {
            @Override
            public void mouseHover(final MouseEvent e) {
                bh.hover = true;
            }

            @Override
            public void mouseExit(final MouseEvent e) {
                bh.hover = false;
            }

            @Override
            public void mouseEnter(final MouseEvent e) {
                bh.hover = true;
            }
        });
        PaintListener pl = e -> {
            GC gcBack = new GC(background);
            gcBack.setAntialias(SWT.ON);
            gcBack.setBackground(bh.hover ? h : c);
            gcBack.setAlpha(255);
            gcBack.fillRectangle(0, 0, b.getBounds().width, b.getBounds().height);
            gcBack.setForeground(t);
            gcBack.setFont(this.font);
            int textWidth = gcBack.getFontMetrics().getAverageCharWidth() * text.length();
            gcBack.drawText(text, (background.getBounds().width - textWidth) >> 1, 8, true);
            gcBack.setAlpha(255);
            e.gc.drawImage(background, 0, 0);
            gcBack.dispose();
        };
        b.addPaintListener(pl);
        b.setFont(this.font);
        b.setText(text);
    }

    public String getFontName() {
        return "media/PetitaMedium";
    }

    public Color getBackgroundColor() {
        return Color.rgb(50);
    }

    public Color getFontColor() {
        return Color.WHITE;
    }

    public Color getButtonColor() {
        return Color.rgb(220, 220, 220);
    }

    public Color getButtonHighlightColor() {
        return Color.rgb(255, 255, 255);
    }

    public Color getButtonTextColor() {
        return Color.rgb(10, 10, 10);
    }

    public ScreenSize getWindowSize() {
        return windowSize;
    }

    public ScreenSize getScreenSize() {
        return screenSize;
    }

    /**
     * Simple wrapper to be used in anonymous class.
     *
     * @author Van den Borre Grégory
     */
    private final class ButtonHover {

        /**
         * Flag if mouse is over button or not.
         */
        private boolean hover = false;
    }
}
