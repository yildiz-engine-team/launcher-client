/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.view.state;

import be.yildizgames.common.logging.LogFactory;
import be.yildizgames.launcher.client.event.Event;
import be.yildizgames.launcher.client.event.EventConstant;
import be.yildizgames.launcher.client.event.EventFactory;
import be.yildizgames.launcher.client.event.EventListener;
import be.yildizgames.launcher.client.event.EventManager;
import be.yildizgames.launcher.client.event.EventRepository;
import be.yildizgames.launcher.client.view.SwtConverter;
import be.yildizgames.launcher.client.view.ViewTemplate;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.slf4j.Logger;

/**
 * Updating is the state to display while a component is updating.
 * Once activated, it displays a progress bar and the current download file name.
 * @author Grégory Van den Borre
 */
public class Updating implements StateWithContext, EventListener {

    private static final Logger LOGGER = LogFactory.getInstance().getLogger(Updating.class);

    /**
     * Display the current file transfer progress.
     */
    private final ProgressBar current;

    /**
     * Name of the file currently transferred.
     */
    private final Label name;

    private final EventRepository repository = new EventRepository();

    public Updating(ViewTemplate template) {
        this.name = template.createLabel(template.getProgressNameCoordinate());
        this.name.setVisible(false);
        this.name.setText("loading");

        this.current = template.createProgressBar();
        this.current.setBounds(SwtConverter.from(template.getProgressCoordinate()));
        this.current.setVisible(false);

        EventManager.getInstance().observe(EventConstant.RECEIVE, this);
    }

    @Override
    public String getName() {
        return EventConstant.STARTING_UPDATE;
    }

    @Override
    public void activate() {
        this.current.setVisible(true);
        this.name.setVisible(true);
    }

    @Override
    public void deactivate() {
        this.current.setVisible(false);
        this.name.setVisible(false);
    }

    @Override
    public void refresh() {
        //unused
    }

    @Override
    public void listen(Event event) {
        LOGGER.debug("Event received: {}", event);
        if(EventFactory.isReceive(event)) {
            this.name.setText(event.getProperty("name"));
            float downloaded = (Float.parseFloat(event.getProperty("transferred")) / Float.parseFloat(event.getProperty("size"))) * 100;
            this.current.setSelection((int)downloaded);
            this.repository.put(event);
        }
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
