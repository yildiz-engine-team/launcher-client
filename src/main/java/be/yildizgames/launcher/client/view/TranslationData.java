/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.view;

import be.yildizgames.common.client.config.Configuration;
import be.yildizgames.common.client.translation.LanguageProvider;
import be.yildizgames.common.client.translation.Translation;
import be.yildizgames.common.client.translation.TranslationKey;
import be.yildizgames.common.util.language.LanguageValue;

/**
 * Provide the translation data for the application.
 * 
 * @author Van den Borre Grégory
 */
public final class TranslationData {

    public static final TranslationKey APP_TITLE =TranslationKey.get("app.title");
    public static final TranslationKey WINDOW_OPTION = TranslationKey.get("window.option");
    public static final TranslationKey BUTTON_ACCEPT = TranslationKey.get("button.accept");
    public static final TranslationKey BUTTON_REFUSE = TranslationKey.get("button.refuse");
    public static final TranslationKey BUTTON_PLAY = TranslationKey.get("button.play");
    public static final TranslationKey BUTTON_OPTION = TranslationKey.get("button.option");
    public static final TranslationKey BUTTON_CANCEL = TranslationKey.get("button.cancel");
    public static final TranslationKey BUTTON_SAVE = TranslationKey.get("button.save");
    public static final TranslationKey TEXT_LOADING = TranslationKey.get("text.loading");
    public static final TranslationKey WARNING_TITLE = TranslationKey.get("warning.title");
    public static final TranslationKey ERROR_GENERAL = TranslationKey.get("error.general");
    public static final TranslationKey ERROR_TITLE = TranslationKey.get("error.title");
    public static final TranslationKey ERROR_JAVA_CONTENT = TranslationKey.get("error.java.content");
    public static final TranslationKey ERROR_JAVA_EXISTS = TranslationKey.get("error.java.exists");
    public static final TranslationKey ERROR_FILE_GAME_404 = TranslationKey.get("error.file.game.404");
    public static final TranslationKey ERROR_HTTP_FILE_RETRIEVE = TranslationKey.get("error.http.file.retrieve");
    public static final TranslationKey ERROR_GENERAL_UPDATE = TranslationKey.get("error.general.update");
    public static final TranslationKey ERROR_ZIP_OPEN = TranslationKey.get("error.zip.open");
    public static final TranslationKey UPDATING = TranslationKey.get("update.run");
    public static final TranslationKey UPDATING_VALUE = TranslationKey.get("update.value");
    public static final TranslationKey ERROR_NEWS_RETRIEVE = TranslationKey.get("error.news.retrieve");
    public static final TranslationKey ERROR_LICENCE_RETRIEVE = TranslationKey.get("error.licence.retrieve");
    public static final TranslationKey BUTTON_INSTALL = TranslationKey.get("button.install");

    private TranslationData() {
        super();
    }

    public static void initialize() {
        LanguageProvider provider = new LanguageProvider();
        provider.add(APP_TITLE, "Yildiz Gestionnaire de mise à jour", "Yildiz Update manager");
        provider.add(WINDOW_OPTION, "Options du jeu", "Game options");
        provider.add(BUTTON_ACCEPT, "Accepter", "Accept");
        provider.add(BUTTON_REFUSE, "Refuser", "Refuse");
        provider.add(BUTTON_PLAY, "Jouer", "Play");
        provider.add(BUTTON_OPTION, "Options", "Options");
        provider.add(BUTTON_CANCEL, "Annuler", "Cancel");
        provider.add(BUTTON_SAVE, "Sauvegarder", "Save");
        provider.add(BUTTON_INSTALL, "Installer", "Install");
        provider.add(TEXT_LOADING, "Chargement,  veuillez patienter", "Loading, please wait");
        provider.add(WARNING_TITLE, "Attention", "Warning");
        provider.add(ERROR_GENERAL, "Une erreur est survenue", "An error occurred");
        provider.add(ERROR_TITLE, "Une erreur est survenue", "An error occurred");
        provider.add(ERROR_JAVA_CONTENT, "La version de java n'a pu être définie.", "Java version could not be defined.");
        provider.add(ERROR_JAVA_EXISTS, "L'executable java n'a pu être trouvé.", "Java binary was not found.");
        provider.add(ERROR_FILE_GAME_404, "Le fichier de lancement du jeu n'a pu être trouvé, relancez le launcher pour le retélécharger.", "The game file executable has not been found, please restart the launcher to download it.");
        provider.add(ERROR_HTTP_FILE_RETRIEVE, "Le fichier n'a pu être récupéré sur le serveur", "The file could not be retrieved on the server.");
        provider.add(ERROR_GENERAL_UPDATE, "Une erreur s'est produite durant la vérification de version", "An error occurred during the version checking");
        provider.add(ERROR_ZIP_OPEN, "Impossible de décompresser jre.zip", "Cannot unzip jre.zip");
        provider.add(UPDATING, "Mise à jour de ${0} en cours", "Updating ${0}");
        provider.add(UPDATING_VALUE, "Une mise à jour de ${0} est disponible", "An update for ${0} is available.");
        provider.add(ERROR_NEWS_RETRIEVE, "Impossible de récupérer le flux de news.", "News stream cannot be retrieved.");
        provider.add(ERROR_LICENCE_RETRIEVE, "Impossible de récupérer le flux de licence.", "Licence stream cannot be retrieved.");
        Translation.getInstance().addLanguage(LanguageValue.EN, provider);
        Translation.getInstance().addLanguage(LanguageValue.FR, provider);
        Translation.getInstance().chooseLanguage(Configuration.getInstance().getLanguage());
    }
}
