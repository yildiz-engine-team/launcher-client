package be.yildizgames.launcher.client.view;

public class WidgetSize {

    public final int width;

    public final int height;

    public WidgetSize(int width, int height) {
        this.width = width;
        this.height = height;
    }
}
