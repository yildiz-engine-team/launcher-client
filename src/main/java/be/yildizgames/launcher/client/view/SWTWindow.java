/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.view;

import be.yildizgames.common.client.config.Configuration;
import be.yildizgames.common.client.translation.Translation;
import be.yildizgames.launcher.client.data.ContentProvider;
import be.yildizgames.launcher.client.event.Event;
import be.yildizgames.launcher.client.event.EventConstant;
import be.yildizgames.launcher.client.event.EventFactory;
import be.yildizgames.launcher.client.event.EventListener;
import be.yildizgames.launcher.client.event.EventManager;
import be.yildizgames.module.coordinate.Coordinates;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * SWT implementation for the launcher window.
 *
 * @author Van den Borre Grégory
 */
public final class SWTWindow implements Window, EventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(SWTWindow.class);

    private final Shell shell;

    /**
     * Area to display the news.
     */
    private final Text textArea;

    private final ContentProvider provider;

    private final ViewTemplate template;

    /**
     * Flag to check if the application is running or not.
     */
    private boolean running;

    /**
     * Build the window.
     *
     * @param provider   Provide licence and news.
     */
    public SWTWindow(ContentProvider provider) {
        super();
        System.setProperty("SWT_GTK3", "0");
        LOGGER.debug("Preparing SWT window");
        this.shell = new Shell(Display.getCurrent(), SWT.NO_TRIM | SWT.TRANSPARENT);
        this.provider = provider;
        this.template = new ViewTemplate(this.shell);

        this.shell.setText("Yildiz update manager");
        Display.setAppName("Yildiz update manager");

        this.shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
        this.shell.setBackground(SwtConverter.from(this.template.getBackgroundColor()));
        this.shell.setBounds(SwtConverter.from(this.template.getWindowCoordinates()));

        Listener l = new Listener() {
            Point origin;

            @Override
            public void handleEvent(final org.eclipse.swt.widgets.Event e) {
                switch (e.type) {
                    case SWT.MouseDown:
                        this.origin = new Point(e.x, e.y);
                        break;
                    case SWT.MouseUp:
                        this.origin = null;
                        break;
                    case SWT.MouseMove:
                        if (origin != null) {
                            Point p = shell.getDisplay().map(shell, null, e.x, e.y);
                            shell.setLocation(p.x - this.origin.x, p.y - this.origin.y);
                        }
                        break;
                    default:
                        throw new IllegalArgumentException("Invalid switch case: " + e);
                }
            }
        };
        this.shell.addListener(SWT.MouseDown, l);
        this.shell.addListener(SWT.MouseUp, l);
        this.shell.addListener(SWT.MouseMove, l);

        Label versionLabel = this.template.createLabel(
                shell.getBounds().width - this.template.divideWindowWidth(8),
                shell.getBounds().height - this.template.divideWindowHeight(20));
        versionLabel.setText("Version: " + (this.getClass().getPackage().getImplementationVersion() != null ?
                this.getClass().getPackage().getImplementationVersion() :
                "1.0.0"));

        this.textArea = this.template.createTextArea();
        this.textArea.setBounds(SwtConverter.from(this.template.getTextCoordinates()));

        this.textArea.setVisible(true);
        this.setLogo();
        this.setIcon();
        this.setQuitButton();

        EventManager.getInstance()
                .observe(EventConstant.CLOSE, this)
                .observe(EventConstant.ERROR, this)
                .observe(EventConstant.WARNING, this)
                .observe(EventConstant.OPTIONS, this)
                .observe(EventConstant.LICENCE, this)
                .observe(EventConstant.READY, this);

        shell.open();
    }

    /*private void activateTemplate(ViewTemplate template) {
        this.shell.setBackground(SwtConverter.from(template.getBackgroundColor()));
    }*/

    private void setIcon() {
        this.shell.setImage(this.getImage("yo.ico"));
    }

    private void setLogo() {
        Label label = new Label(this.shell, SWT.NONE);
        label.setBounds(SwtConverter.from(this.template.getLogoCoordinates()));
        label.setImage(this.getImage("logo_tr_256.png"));
        label.pack();
    }

    private void setQuitButton() {
        final Button quit = this.createButton("cnx_cs.png", "cnx_cs_h.png");
        quit.setBounds(SwtConverter.from(this.template.getQuitButtonCoordinates()));
        quit.setVisible(true);
        quit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDown(final MouseEvent event) {
                close();
            }
        });
    }

    /**
     * Update the view.
     */
    @Override
    public void run() {
        if (!this.running) {
            this.running = true;
            while (this.running) {
                if (!this.shell.getDisplay().readAndDispatch()) {
                    this.shell.getDisplay().sleep();
                }
            }
            LOGGER.debug("Closing window");
            this.shell.dispose();
            Display.getDefault().close();
        }
    }

    private void close() {
        this.running = false;
    }

    private void displayError(String messageKey) {
        MessageBox error = new MessageBox(this.shell, SWT.OK);
        error.setText(Translation.getInstance().translate(TranslationData.ERROR_TITLE));
        error.setMessage(messageKey);
        if (error.open() == SWT.OK) {
            this.close();
        }
    }

    private void displayWarning(String messageKey) {
        MessageBox error = new MessageBox(this.shell, SWT.NONE);
        error.setText(Translation.getInstance().translate(TranslationData.WARNING_TITLE));
        error.setMessage(messageKey);
    }

    /**
     * Create an image from a file stored in media.
     *
     * @param file File path.
     * @return The created image.
     */
    private Image getImage(final String file) {
        return this.getImageFromFile(file);
    }

    private Image getImageFromFile(final String file) {
        try {
            Path imageFile = Paths.get("media/" + file).toAbsolutePath();
            InputStream image = Files.newInputStream(imageFile);
            return new Image(this.shell.getDisplay(), image);
        } catch (IOException e) {
            LOGGER.error("Exception trying to retrieve image from file", e);
            return this.getImageFromContentProvider(file);
        }
    }

    private Image getImageFromContentProvider(final String file) {
        try {
            Path imageFile = Paths.get("media/" + file).toAbsolutePath();
            Files.createDirectories(imageFile.getParent());
            InputStream image = this.provider.getFile("launcher/" + file);
            Files.copy(image, imageFile);
            image = Files.newInputStream(imageFile);
            return new Image(this.shell.getDisplay(), image);
        } catch (IOException e) {
            LOGGER.error("Exception trying to retrieve image from content provider", e);
            return this.getEmptyImage(file);
        }
    }

    private Image getEmptyImage(String file) {
        LOGGER.warn("Using empty image instead of {}", file);
        return new Image(this.shell.getDisplay(), 16, 16);
    }

    private Button createButton(final String background, final String hover) {
        return this.createButton(this.getImage(background), this.getImage(hover));
    }

    private Button createButton(final Image background, final Image hover) {
        Button button = new Button(this.shell, SWT.SMOOTH);
        button.setImage(background);
        button.addListener(SWT.MouseEnter, e -> button.setImage(hover));
        button.addListener(SWT.MouseExit, e -> button.setImage(background));
        return button;
    }

    @Override
    public void listen(Event event) {
        LOGGER.debug("Event received in swt window: {}", event);
        switch (event.name) {
            case EventConstant.REGISTER_GAME:
                this.registerGame(event);
                break;
            case EventConstant.OPTIONS:
                this.refresh();
                break;
            case EventConstant.CLOSE:
                this.close();
                break;
            case EventConstant.ERROR:
                this.displayError(event.getProperty("message"));
                break;
            case EventConstant.WARNING:
                this.displayWarning(event.getProperty("message"));
                break;
            case EventConstant.LICENCE:
                this.textArea.setVisible(true);
                this.shell.setText("End User License Agreement");
                this.textArea.setText(this.provider.getLicence(Configuration.getInstance().getLanguage()));
                break;
            case EventConstant.READY:
                this.textArea.setVisible(true);
                this.shell.setText("Yildiz update manager");
                this.textArea.setText(this.provider.getNews(Configuration.getInstance().getLanguage()));
                break;
        }

    }

    private void registerGame(Event event) {
            Button b = this.createButton(event.getProperty("icon"), event.getProperty("icon-hover"));
            Coordinates c = this.template.getGameIconCoordinate(Integer.valueOf(event.getProperty("order")));
            b.setBounds(c.left, c.top, c.width, c.height);
            b.setVisible(true);
            String name = event.getProperty("type");
            b.addMouseListener(new MouseAdapter() {

                @Override
                public void mouseDown(MouseEvent e) {
                    EventManager.getInstance().fireEvent(EventFactory.activateGame(name));
                }
            });
        }

    @Override
    public void refresh() {
        this.textArea.setVisible(true);
        this.textArea.setText(this.provider.getNews(Configuration.getInstance().getLanguage()));
    }

    public ViewTemplate getTemplate() {
        return template;
    }
}
