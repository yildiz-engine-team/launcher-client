/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.view.state;

import be.yildizgames.launcher.client.event.*;
import be.yildizgames.launcher.client.game.SupportedGameList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Grégory Van den Borre
 */
public class StateMachine implements EventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(StateMachine.class);
    
    private final SupportedGameList gameList;

    private StateWithContext current = new Empty();

    private Map<String, StateWithContext> states = new HashMap<>();

    private String blocked;

    private String currentGame;

    public StateMachine(SupportedGameList gameList) {
        super();
        this.states.put("empty", current);
        this.gameList = gameList;

        EventManager.getInstance()
                .observe(EventConstant.OPTIONS, this)
                .observe(EventConstant.READY, this)
                .observe(EventConstant.CHECKING, this)
                .observe(EventConstant.NEED_UPDATE, this)
                .observe(EventConstant.STARTING_UPDATE, this)
                .observe(EventConstant.BLOCKING_UPDATE, this)
                .observe(EventConstant.ACTIVE_GAME, this);
    }

    public StateMachine addState(StateWithContext s) {
        this.states.put(s.getName(), s);
        return this;
    }

    private void setState(String state, String context) {
        states.values().forEach(State::deactivate);
        if(this.blocked == null || context.equals("java") || context.equals("launcher")) {
            LOGGER.debug("Setting state: {} for {} instead of {}", state, context, this.current);
            this.current = states.get(state);
            this.current.setContext(context);
            this.current.activate();
        }
    }


    @Override
    public void listen(Event event) {
        LOGGER.debug("Event received: {}", event);
        if(EventFactory.isOption(event)) {
            this.states.values().forEach(StateWithContext::refresh);
        } else if (event.name.equals(EventConstant.ACTIVE_GAME)) {
            this.currentGame = event.getProperty(EventConstant.PROPERTY_TYPE);
        } else if(event.name.equals(EventConstant.BLOCKING_UPDATE)) {
            this.blocked = event.getProperty(EventConstant.PROPERTY_TYPE);
        } else if(event.name.equals(EventConstant.READY) && currentGame.equals(event.getProperty(EventConstant.PROPERTY_TYPE))) {
            String game = event.getProperty(EventConstant.PROPERTY_TYPE);
            this.gameList.byName(game).ifPresent(
                        g -> setState(event.name, event.getProperty(EventConstant.PROPERTY_TYPE)));
        } else {
            this.setState(event.name, event.getProperty(EventConstant.PROPERTY_TYPE));
        }
    }
}
