/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.view.state;

import be.yildizgames.launcher.client.event.EventConstant;
import be.yildizgames.launcher.client.event.EventFactory;
import be.yildizgames.launcher.client.event.EventManager;
import be.yildizgames.launcher.client.view.TranslationData;
import be.yildizgames.launcher.client.view.ViewTemplate;
import be.yildizgames.common.client.translation.Translation;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;

/**
 * @author Grégory Van den Borre
 */
public class NeedUpdate implements StateWithContext {

    private final Button refuse;
    private final Label label;
    private final ViewTemplate template;
    private Button accept;
    private boolean active;

    private String update;

    public NeedUpdate(ViewTemplate template) {
        this.template = template;
        this.accept = template.createButton(template.getLeftButtonCoordinates(), Translation.getInstance().translate(TranslationData.BUTTON_ACCEPT));
        this.accept.setVisible(false);
        this.refuse = template.createButton(template.getRightButtonCoordinates(), Translation.getInstance().translate(TranslationData.BUTTON_REFUSE));
        this.refuse.setVisible(false);
        this.refuse.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseDown(final MouseEvent event) {
                EventManager.getInstance().fireEvent(EventFactory.close());
            }
        });
        this.accept.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseDown(MouseEvent arg0) {
                EventManager.getInstance().fireEvent(EventFactory.updating(update));
            }
        });
        this.label = template.createLabel(50, template.getWindowSize().height - 50);
        this.label.setVisible(false);
    }

    @Override
    public String getName() {
        return EventConstant.NEED_UPDATE;
    }

    @Override
    public void activate() {
        this.active = true;
        EventManager.getInstance().fireEvent(EventFactory.licence());
        accept.setVisible(true);
        refuse.setVisible(true);
        this.label.setVisible(true);
    }

    @Override
    public void setContext(String context) {
        this.label.setText(Translation.getInstance().translate(TranslationData.UPDATING_VALUE, context));
        this.update = context;
    }

    @Override
    public void deactivate() {
        this.active = false;
        accept.setVisible(false);
        refuse.setVisible(false);
        this.label.setVisible(false);
    }

    @Override
    public void refresh() {
        if(this.active) {
            EventManager.getInstance().fireEvent(EventFactory.licence());
        }
        this.template.setText(this.accept, Translation.getInstance().translate(TranslationData.BUTTON_ACCEPT));
        this.template.setText(this.refuse, Translation.getInstance().translate(TranslationData.BUTTON_REFUSE));
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
