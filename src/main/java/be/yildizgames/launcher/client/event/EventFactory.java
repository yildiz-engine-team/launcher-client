/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.event;

import be.yildizgames.launcher.client.game.SupportedGame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Grégory Van den Borre
 */
public class EventFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventFactory.class);

    private EventFactory() {
        super();
    }

    public static Event close() {
        LOGGER.debug("Create close event");
        return new Event(EventConstant.CLOSE);
    }

    public static Event ready(String name) {
        LOGGER.debug("Create ready event {}", name);
        return new Event(EventConstant.READY)
                .addProperty(EventConstant.PROPERTY_TYPE, name);
    }

    public static Event receive(final String name, float transferred, float length, float size) {
        return new Event(EventConstant.RECEIVE)
                .addProperty("name", name)
                .addProperty("transferred", String.valueOf(transferred))
                .addProperty("length", String.valueOf(length))
                .addProperty("size", String.valueOf(size));
    }

    public static Event needUpdate(String name) {
        LOGGER.debug("Create need update event {}", name);
        return new Event(EventConstant.NEED_UPDATE)
                .addProperty(EventConstant.PROPERTY_TYPE, name);
    }

    public static Event blockingUpdate(String name) {
        LOGGER.debug("Create need update blocking event {}", name);
        return new Event(EventConstant.BLOCKING_UPDATE).addProperty(EventConstant.PROPERTY_TYPE, name);
    }

    public static Event licence() {
        LOGGER.debug("Create licence event");
        return new Event(EventConstant.LICENCE);
    }

    public static Event updating(String name) {
        LOGGER.debug("Create starting update event {}", name);
        return new Event(EventConstant.STARTING_UPDATE).addProperty(EventConstant.PROPERTY_TYPE, name);
    }

    public static Event option() {
        return new Event(EventConstant.OPTIONS);
    }

    public static Event activateGame(String name) {
        LOGGER.debug("Create activate event {}", name);
        return new Event(EventConstant.ACTIVE_GAME)
                .addProperty(EventConstant.PROPERTY_TYPE, name);
    }

    public static Event install(String name) {
        LOGGER.debug("Create install event {}", name);
        return new Event(EventConstant.INSTALL_GAME)
                .addProperty(EventConstant.PROPERTY_TYPE, name);
    }

    public static boolean isOption(Event event) {
        return event.name.equals(EventConstant.OPTIONS);
    }

    public static boolean isReceive(Event event) {
        return event.name.equals(EventConstant.RECEIVE);
    }

    public static Event checkUpdate(String name) {
        LOGGER.debug("Create check for update event {}", name);
        return new Event(EventConstant.CHECKING)
                .addProperty(EventConstant.PROPERTY_TYPE, name);
    }

    public static Event registerGame(SupportedGame game, int order) {
        LOGGER.debug("Create register game event for {}", game.getName());
        return new Event(EventConstant.REGISTER_GAME)
                .addProperty(EventConstant.PROPERTY_TYPE, game.getName())
                .addProperty("order", order);
    }
}
