/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.event;

/**
 * @author Grégory Van den Borre
 */
public class EventConstant {

    public static final String ERROR = "error";

    /**
     * Event fired when the licenced must be displayed.
     */
    public static final String LICENCE = "licence";

    /**
     * Event fired when the state changes to updated.
     */
    public static final String STARTING_UPDATE = "updating";

    /**
     * Event fired when the launcher need to be closed.
     */
    public static final String CLOSE = "close";

    /**
     * Event fired when the state changes to ready.
     */
    public static final String READY = "ready";

    public static final String RECEIVE = "receive";

    /**
     * Event fired when the state changes to need-update.
     */
    public static final String NEED_UPDATE = "need-update";

    /**
     * Event fired when the options are changed.
     */
    public static final String OPTIONS = "option";

    /**
     * Event fired when the state changes to checking.
     */
    public static final String CHECKING = "checking";

    public static final String WARNING = "warning";

    public static final String ACTIVE_GAME = "activate-game";

    public static final String INSTALL_GAME = "install-game";

    public static final String BLOCKING_UPDATE = "blocking-update";

    public static final String PROPERTY_TYPE = "type";
    public static final String REGISTER_GAME = "register-game";

    private EventConstant() {
        super();
    }
}
