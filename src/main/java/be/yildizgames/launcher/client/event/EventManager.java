/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.event;

import be.yildizgames.common.client.translation.Translation;
import be.yildizgames.common.client.translation.TranslationKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Grégory Van den Borre
 */
public class EventManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventManager.class);

    private static final EventManager INSTANCE = new EventManager();

    private Map<String, List<EventListener>> listeners = new HashMap<>();

    private EventManager() {
        super();
    }

    public static EventManager getInstance() {
        return INSTANCE;
    }

    public EventManager observe(String event, EventListener... l) {
        assert event != null;
        assert  l != null;
        List<EventListener> list = listeners.computeIfAbsent(event, k -> new ArrayList<>());
        list.addAll(Arrays.asList(l));
        return this;
    }

    public EventManager fireEvent(Event event) {
        assert event != null;
        LOGGER.debug("Fire event: {}", event.name);
        Optional.ofNullable(listeners.get(event.name))
                .ifPresent(list -> list.forEach(e -> e.listen(event)));
        return this;
    }

    public EventManager fireError(String message) {
        assert message != null;
        Event e = new Event("error");
        e.addProperty("message", message);
        LOGGER.debug("Fire error:{}", message);
        return this.fireEvent(e);
    }

    public EventManager fireError(TranslationKey message) {
        assert message != null;
        return this.fireError(Translation.getInstance().translate(message));
    }

    public EventManager fireWarning(String message) {
        assert message != null;
        Event e = new Event("warning");
        e.addProperty("message", message);
        LOGGER.debug("Fire warning:{}", message);
        return this.fireEvent(e);
    }

    public EventManager fireWarning(TranslationKey message) {
        return this.fireWarning(Translation.getInstance().translate(message));
    }
}
