/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.main;

import be.yildizgames.common.client.config.Configuration;
import be.yildizgames.common.file.FileProperties;
import be.yildizgames.launcher.client.ThreadManager;
import be.yildizgames.launcher.client.event.EventManager;
import be.yildizgames.launcher.client.game.SpideyGame;
import be.yildizgames.launcher.client.game.SupportedGameList;
import be.yildizgames.launcher.client.game.YildizOnlineGame;
import be.yildizgames.launcher.client.network.NetworkClient;
import be.yildizgames.launcher.client.network.NetworkClientFactory;
import be.yildizgames.launcher.client.network.ServerException;
import be.yildizgames.launcher.client.script.ExternalScriptBuilder;
import be.yildizgames.launcher.client.transfer.UpdateManager;
import be.yildizgames.launcher.client.view.SWTWindow;
import be.yildizgames.launcher.client.view.TranslationData;
import be.yildizgames.launcher.client.view.state.CheckingFiles;
import be.yildizgames.launcher.client.view.state.NeedUpdate;
import be.yildizgames.launcher.client.view.state.Ready;
import be.yildizgames.launcher.client.view.state.StateMachine;
import be.yildizgames.launcher.client.view.state.Updating;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Launcher client entry point.
 *
 * @author Van den Borre Grégory
 */
public final class EntryPoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(EntryPoint.class);

    private EntryPoint() {
        super();
    }

    /**
     * Application entry point, initialize the view and the network.
     *
     * @param args Unused.
     */
    public static void main(String[] args) {
        try {
            Path configFile = Paths.get("config.properties");
            LOGGER.debug("Reading property file {} ...", configFile.toAbsolutePath());
            Configuration.getInstance().readFromProperties(FileProperties.getOrCreatePropertiesFromFile(configFile));
            TranslationData.initialize();
            LOGGER.debug("Property file loaded.");

            SupportedGameList supportedGames = SupportedGameList
                    .create()
                    .register(YildizOnlineGame.create())
                    .register(SpideyGame.create());

            deleteOldFiles();

            NetworkClient client = NetworkClientFactory.create();
            SWTWindow window = new SWTWindow(client);
            new ExternalScriptBuilder().run(supportedGames);

            UpdateManager updateManager = new UpdateManager(client, supportedGames);

            new StateMachine(supportedGames)
                    .addState(new CheckingFiles(window.getTemplate()))
                    .addState(new NeedUpdate(window.getTemplate()))
                    .addState(new Updating(window.getTemplate()))
                    .addState(new Ready(window.getTemplate(), supportedGames));

            ThreadManager.runAsync(updateManager);
            LOGGER.debug("Starting launcher");
            window.run();
        } catch (ServerException e) {
            LOGGER.error("Error", e);
            EventManager.getInstance().fireError(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Unhandled exception", e);
            EventManager.getInstance().fireError("error.general");
        }
    }


    /**
     * Delete previous files used as temporary when updating the launcher or the JVM.
     * Side effect: Delete files on disk, if they exist.
     */
    private static void deleteOldFiles() {
        Stream.of("launcher_old", "../jre_old")
                .map(Paths::get)
                .filter(Files::exists)
                .forEach(path -> {
                    try {
                        Files.delete(path);
                    } catch (IOException e) {
                        LOGGER.error("File not deleted: {}", path, e);
                    }
                });
    }
}
