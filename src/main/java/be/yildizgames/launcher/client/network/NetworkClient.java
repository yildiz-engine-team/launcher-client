/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.network;

import be.yildizgames.common.file.ResourceUtil;
import be.yildizgames.common.os.OperatingSystem;
import be.yildizgames.common.os.factory.OperatingSystems;
import be.yildizgames.common.util.language.Language;
import be.yildizgames.launcher.client.data.ContentProvider;
import be.yildizgames.launcher.client.event.EventFactory;
import be.yildizgames.launcher.client.event.EventManager;
import be.yildizgames.launcher.client.view.TranslationData;
import be.yildizgames.launcher.shared.files.FileDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Base class for a client to connect to the file server. This base
 * implementation provide everything excepted for the request to the server
 * witch is implementation specific.
 *
 * @author Van den Borre Grégory
 */
public abstract class NetworkClient implements ContentProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(NetworkClient.class);

    /**
     * Buffer size.
     */
    private static final int BUFFER_SIZE = 1024;

    /**
     * Address to contact the server.
     */
    private final String address;

    private final OperatingSystem system = OperatingSystems.getCurrent();

    /**
     * Flag to have the network running in debug mode or not, in debug mode, the
     * java version is not checked..
     */
    private boolean debug;

    /**
     * Full constructor, create the address to contact from the given address
     * and port.
     *
     * @param address      Server to contact address.
     */
    protected NetworkClient(final String address) {
        super();
        this.address = address;
    }

    public final void request(String context, final FileDescription f, Path fileOutput) {
        try {
            this.request(context, f.name, f.size, fileOutput);
        } catch (Exception e) {
            LOGGER.error("Error in request", e);
        }
    }

    /**
     * Request a file from the server.
     *
     * @param context File context.
     * @param toRequest File to request.
     * @param size      Size of the file, in bytes.
     * @param fileOutput File to write.
     * @throws Exception If something went wrong.
     */
    public final void request(String context, final String toRequest, final long size, final Path fileOutput) throws Exception {
        Path parentFile = fileOutput.getParent();
        if (parentFile != null) {
            Files.createDirectories(parentFile);
        }
        try (
                BufferedInputStream bis = new BufferedInputStream(this.getStream(context + "/" + system.getName() + "/" + toRequest));
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(fileOutput.toFile()))) {
            byte[] buf = new byte[BUFFER_SIZE];
            int len;
            long currentlyTransferred = 0;
            while ((len = bis.read(buf)) > 0) {
                bos.write(buf, 0, len);
                currentlyTransferred += len;
                EventManager.getInstance().fireEvent(EventFactory.receive(fileOutput.toString(), currentlyTransferred, len, size));
            }
        }
    }

    /**
     * Request the text content of a file.
     *
     * @param distantFile Distant file to read.
     * @return The content of the file.
     * @throws Exception If something went wrong.
     */
    public final String retrieveText(final String distantFile) throws Exception {
        try (
                BufferedInputStream bis = new BufferedInputStream(this.getStream(distantFile));
                ByteArrayOutputStream bos = new ByteArrayOutputStream(BUFFER_SIZE)) {
            byte[] buf = new byte[BUFFER_SIZE];
            int len;
            while ((len = bis.read(buf)) > 0) {
                bos.write(buf, 0, len);
            }
            return ResourceUtil.getString(bos);
        }
    }

    @Override
    public final String getLicence(Language language) {
        try {
            return this.retrieveText("licence/" + language.getShortName() + "/text");
        } catch (Exception e) {
            EventManager.getInstance().fireError(TranslationData.ERROR_LICENCE_RETRIEVE);
            return "";
        }
    }

    @Override
    public final InputStream getFile(String file) {
        try {
            return this.getStream(file);
        } catch (Exception e) {
            EventManager.getInstance().fireError(TranslationData.ERROR_HTTP_FILE_RETRIEVE);
            return null;
        }
    }

    @Override
    public final String getNews(Language language) {
        try {
            return this.retrieveText("launcher/news/" + language.getShortName() + "/index.html");
        } catch (Exception e) {
            EventManager.getInstance().fireError(TranslationData.ERROR_NEWS_RETRIEVE);
            return "";
        }
    }

    /**
     * Call to an HTTP get method, return the stream generated by the response.
     *
     * @param file File to request.
     * @return The stream for the request file.
     * @throws Exception If an exception occurs.
     */
    private InputStream getStream(final String file) throws Exception {
        String request = this.address + URLEncoder.encode(file, StandardCharsets.UTF_8.name()).replace("%2F", "/");
        LOGGER.debug("Requesting {}", request);
        return this.getStreamImpl(new URI(request));
    }

    /**
     * Implementation specific call to an HTTP get method, return the stream
     * generated by the response.
     *
     * @param file File to request.
     * @return The stream for the request file.
     * @throws IOException If an exception occurs.
     */
    protected abstract InputStream getStreamImpl(URI file) throws IOException;
}
