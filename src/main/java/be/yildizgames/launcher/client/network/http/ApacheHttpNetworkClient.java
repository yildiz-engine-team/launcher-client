/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.network.http;

import be.yildizgames.common.client.config.Configuration;
import be.yildizgames.launcher.client.network.NetworkClient;
import be.yildizgames.launcher.client.network.ServerException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.NoRouteToHostException;
import java.net.URI;

/**
 * Apache HTTP client implementation.
 *
 * @author Van den Borre Grégory
 */
public class ApacheHttpNetworkClient extends NetworkClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApacheHttpNetworkClient.class);

    /**
     * Full constructor, create the address to contact from the given address and port.
     */
    public ApacheHttpNetworkClient() {
        super(Configuration.getInstance().getServerHost());
    }

    @Override
    protected InputStream getStreamImpl(final URI url) throws IOException {
        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpGet httpget = new HttpGet(url);
        try {
            HttpResponse response = httpclient.execute(httpget);
            int code = response.getStatusLine().getStatusCode();
            if (code != 200) {
                LOGGER.error("Error retrieving file: {}", url.toString());
                throw new ServerException("error.http.file.retrieve");
            }
            HttpEntity entity = response.getEntity();
            return entity.getContent();
        } catch (ClientProtocolException | HttpHostConnectException | NoRouteToHostException e) {
            LOGGER.error("Error retrieving file: {}", url, e);
            throw new ServerException("error.http.file.retrieve", e);
        }
    }
}
