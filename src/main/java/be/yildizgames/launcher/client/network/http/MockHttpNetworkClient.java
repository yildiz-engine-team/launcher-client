/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.network.http;

import be.yildizgames.common.client.config.Configuration;
import be.yildizgames.launcher.client.network.NetworkClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

/**
 * @author Grégory Van den Borre
 */
public class MockHttpNetworkClient extends NetworkClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(MockHttpNetworkClient.class);

    public static final String WRONG_JAVA_VERSION = "wrong-java-version";

    public static final String WRONG_VERSION = "wrong-version";

    public static final String WRONG_YILDIZ = "wrong-yildiz";

    private final List<String> options;

    /**
     * Full constructor, create the address to contact from the given address
     * and port.
     *
     * @param options      Possible options to use.
     */
    public MockHttpNetworkClient(List<String> options) {
        super(Configuration.getInstance().getServerHost());
        this.options = options;
    }

    @Override
    protected InputStream getStreamImpl(URI file) throws IOException {
        if (file.toString().endsWith("version-launcher")) {
            if (this.options.contains(WRONG_VERSION)) {
                LOGGER.debug("Requesting wrong version");
                return arrayFromString("0.0.0");
            }
            LOGGER.debug("Requesting right version");
            return arrayFromString("0.0.0-DEV");
        }
        if (file.toString().endsWith("list.xml")) {
            if (file.toString().contains("Yildiz-Online") && this.options.contains(WRONG_YILDIZ)) {
                LOGGER.debug("Requesting missing Yildiz-Online game file");
                return arrayFromString("<files><file><name>yildiz.file</name><crc>0</crc><size>100000</size></file></files>");
            }
            return arrayFromString("<files></files>");
        }
        if (file.toString().endsWith("version-java")) {
            if (this.options.contains(WRONG_JAVA_VERSION)) {
                LOGGER.debug("Requesting wrong java version");
                return arrayFromString("1");
            }
            return arrayFromString("0");
        }
        if (file.toString().endsWith("text")) {
            return arrayFromString("Dummy licence text");
        }
        if (file.toString().endsWith("jre.zip")) {
            byte[] b = new byte[50000000];
            byte value = 1;
            Arrays.fill(b, value);
            return new ByteArrayInputStream(b);
        }
        if (file.toString().endsWith("index.html")) {
            return arrayFromString("Those are dummy news...");
        }
        if (file.toString().endsWith("yildiz.file")) {
            byte[] b = new byte[50000000];
            byte value = 1;
            Arrays.fill(b, value);
            return new ByteArrayInputStream(b);
        }
        LOGGER.debug("Requesting:{}", file);
        return arrayFromString("ok");
    }

    private ByteArrayInputStream arrayFromString(String v ) {
        return new ByteArrayInputStream(v.getBytes(StandardCharsets.UTF_8));
    }
}
