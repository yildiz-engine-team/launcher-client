/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.client.event;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Grégory Van den Borre
 */
class EventTest {

    @Nested
    class Constructor {

        @Test
        void happyFlow() {
            Event e = new Event("tt");
            assertEquals("tt", e.name);
        }

        @Test
        void withNull() {
            assertThrows(AssertionError.class, () -> new Event(null));
        }
    }

    @Nested
    class AddProperty {

        @Test
        void happyFlow() {
            Event e = new Event("ok");
            e.addProperty("p1", "v1");
            assertEquals("v1", e.getProperty("p1"));
        }

        @Test
        void withNullKey() {
            Event e = new Event("ok");
            assertThrows(AssertionError.class, () -> e.addProperty(null, "v1"));
        }

        @Test
        void withNullValue() {
            Event e = new Event("ok");
            assertThrows(AssertionError.class, () -> e.addProperty("p1", null));
        }
    }

    @Nested
    class GetProperty {

        @Test
        void happyFlow()  {
            Event e = new Event("ok");
            e.addProperty("p1", "v1");
            assertEquals("v1", e.getProperty("p1"));
        }

        @Test
        void notFound() {
            Event e = new Event("ok");
            assertEquals("", e.getProperty("p1"));
        }
    }
}
